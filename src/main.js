import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/iview.js'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import axios from 'axios';
import filter from "./filter/filter";
import area from "./enums/area";

Vue.use(ViewUI);

Vue.config.productionTip = false
Vue.prototype.$area = area
Vue.prototype.$axios = axios;

//全局过滤器
Object.keys(filter).forEach(key => {
  Vue.filter(key,filter[key])
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
