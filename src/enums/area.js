export default {
    '1': '艾欧尼亚',
    '2': '祖安',
    '3': '诺克萨斯',
    '4': '班德尔城',
    '5': '皮尔特沃夫',
    '6': '战争学院',
    '7': '巨神峰',
    '8': '雷瑟守备',
    '9': '钢铁烈阳',
    '10': '裁决之地',
    '11': '黑色玫瑰',
    '12': '暗影岛',
    '13': '均衡教派',
    '14': '水晶之痕',
    '15': '影流',
    '16': '守望之海',
    '17': '征服之海',
    '18': '卡拉曼达',
    '19': '皮城警备',
    '20': '比尔吉沃特',
    '21': '德玛西亚',
    '22': '弗雷尔卓德',
    '23': '无谓先锋',
    '24': '恕瑞玛',
    '25': '扭曲丛林',
}