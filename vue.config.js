module.exports = {
    lintOnSave: false,
    publicPath: '/',
    assetsDir: 'static',
    parallel: false,
    devServer: {

    overlay: {

        warning: false,

            errors: false

    }

},
    devServer: {
        historyApiFallback: true,
        hot: true,
        inline: true,
        stats: { colors: true },
        proxy: {
            //匹配代理的url
            '/api': {
                // 目标服务器地址
                target: 'http://139.196.165.135:8082',
                //target: 'http://localhost:8080',
                //路径重写
                pathRewrite: {'^/api' : '/api'},
                changeOrigin: true
            }
        }
    }


}
